
import random
import hashlib

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"
pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('AUTH_KEY',              '" + output +"');"
)

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"

pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('SECURE_AUTH_KEY',       '" + output + "');"
)

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"
pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('LOGGED_IN_KEY',         '" + output + "');"
)

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"
pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('NONCE_KEY',             '" + output + "');"
)

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"

pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('AUTH_SALT',             '" + output + "');"
)

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"
pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('SECURE_AUTH_SALT',      '" + output + "');"
)

contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"
pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('LOGGED_IN_SALT',        '" + output + "');"
)


contents = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$+-*&_)(}{][=:;,.%1234567890"
pw_length = 64
output = ""

for i in range(pw_length):
    next_index = random.randrange(len(contents))
    output = output + contents[next_index]

print(
"define('NONCE_SALT',            '" + output + "');"
)
